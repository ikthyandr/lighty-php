FROM debian:jessie
MAINTAINER Arnab Somadder <arnab.s@lowerentropy.com>

RUN apt-get -y update\
	&& apt-get -y install \
		php5-mysql \
		php5-cgi \
		lighttpd
		
## Enable fastcgi-php then start lighttpd, so changes take effect	
RUN lighty-enable-mod fastcgi\
	&& lighty-enable-mod fastcgi-php\
	&& /etc/init.d/lighttpd start

EXPOSE 80
ENTRYPOINT ["/usr/sbin/lighttpd"]
CMD ["-f","/etc/lighttpd/lighttpd.conf","-D","FOREGROUND"]
